import Link from "@mui/material/Link";
import { useNavigate } from "react-router-dom";

export default function HomePage() {
	const navigate = useNavigate();
	return (
		<>
			<Link
				sx={{ m: 1 }}
				component="button"
				onClick={() => {
					navigate("/login");
				}}
				href="login"
				variant="body2"
			>
				Login
			</Link>
			<Link
				component="button"
				onClick={() => {
					navigate("/register");
				}}
				href="register"
				variant="body2"
			>
				Register
			</Link>
		</>
	);
}

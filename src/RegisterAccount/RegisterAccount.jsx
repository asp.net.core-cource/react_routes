import * as React from "react";
import Avatar from "@mui/material/Avatar";
import Button from "@mui/material/Button";
import CssBaseline from "@mui/material/CssBaseline";
import TextField from "@mui/material/TextField";
import Grid from "@mui/material/Grid";
import Box from "@mui/material/Box";
import LockOutlinedIcon from "@mui/icons-material/LockOutlined";
import Typography from "@mui/material/Typography";
import Container from "@mui/material/Container";
import TextFieldRequired from "../CommonComponent/CommonComponentPage";
import { useSelector, useDispatch } from "react-redux";
import { setRegister } from "../features/auth/authSlice";
import { registerSelector } from "../features/auth/selectors";

const RequiredTextField = TextFieldRequired(TextField);

export default function RegisterAccount() {
	const registerSelected = useSelector(registerSelector);
	const dispatch = useDispatch();

	const handleSubmit = (event) => {
		event.preventDefault();
		const data = new FormData(event.currentTarget);
		console.log({
			firstName: data.get("firstName"),
			lastName: data.get("lastName"),
			email: data.get("email"),
			password: data.get("password"),
		});
	};

	return (
		<Container component="main" maxWidth="xs">
			<CssBaseline />
			<Box
				sx={{
					marginTop: 8,
					display: "flex",
					flexDirection: "column",
					alignItems: "center",
				}}
			>
				<Avatar sx={{ m: 1, bgcolor: "secondary.main" }}>
					<LockOutlinedIcon />
				</Avatar>
				<Typography component="h1" variant="h5">
					Sign up
				</Typography>
				<Box component="form" noValidate onSubmit={handleSubmit} sx={{ mt: 3 }}>
					<Grid container spacing={2}>
						<Grid item xs={12} sm={6}>
							<RequiredTextField
								autoComplete="given-name"
								name="firstName"
								fullWidth
								id="firstName"
								label="First Name"
								autoFocus
								value={registerSelected?.firstName || ""}
								onChange={(event) => {
									dispatch(
										setRegister({
											...registerSelected,
											firstName: event.target.value,
										})
									);
								}}
							/>
						</Grid>
						<Grid item xs={12} sm={6}>
							<RequiredTextField
								fullWidth
								id="lastName"
								label="Last Name"
								name="lastName"
								autoComplete="family-name"
								value={registerSelected?.lastName || ""}
								onChange={(event) => {
									dispatch(
										setRegister({
											...registerSelected,
											lastName: event.target.value,
										})
									);
								}}
							/>
						</Grid>
						<Grid item xs={12}>
							<RequiredTextField
								fullWidth
								id="email"
								label="Email Address"
								name="email"
								autoComplete="email"
								value={registerSelected?.email || ""}
								onChange={(event) => {
									dispatch(
										setRegister({
											...registerSelected,
											email: event.target.value,
										})
									);
								}}
							/>
						</Grid>
						<Grid item xs={12}>
							<RequiredTextField
								fullWidth
								name="password"
								label="Password"
								type="password"
								id="password"
								autoComplete="new-password"
								value={registerSelected?.password || ""}
								onChange={(event) => {
									dispatch(
										setRegister({
											...registerSelected,
											password: event.target.value,
										})
									);
								}}
							/>
						</Grid>
					</Grid>
					<Button
						type="submit"
						fullWidth
						variant="contained"
						sx={{ mt: 3, mb: 2 }}
					>
						Sign Up
					</Button>
				</Box>
			</Box>
		</Container>
	);
}

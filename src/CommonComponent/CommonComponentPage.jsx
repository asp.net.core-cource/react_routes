import React from "react";

const TextFieldRequired = (WrappedComponent) => (props) => {
	return <WrappedComponent required {...props} />;
};

export default TextFieldRequired;

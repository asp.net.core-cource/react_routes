import { createSelector } from "@reduxjs/toolkit";

export const loginSelector = createSelector(
	(state) => state?.auth,
	(state) => state?.login
);
export const registerSelector = createSelector(
	(state) => state?.auth,
	(state) => state?.register
);

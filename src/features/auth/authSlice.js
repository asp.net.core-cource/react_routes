import { createSlice } from "@reduxjs/toolkit";

const initialState = {
	login: { email: "", password: "" },
	register: { firstName: "", lastName: "", email: "", password: "" },
};

export const authSlice = createSlice({
	name: "auth",
	initialState,
	reducers: {
		setLogin: (state, action) => {
			state.login = action.payload;
		},
		setRegister: (state, action) => {
			state.register = action.payload;
		},
		reset: (state) => {
			state = initialState;
		},
	},
});

export const { setLogin, setRegister } = authSlice.actions;

export default authSlice.reducer;

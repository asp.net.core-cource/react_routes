import "./App.css";
import { BrowserRouter, Routes, Route } from "react-router-dom";
import HomePage from "./HomePage/HomePage";
import LoginAccount from "./LoginAccount/LoginAccount";
import RegisterAccount from "./RegisterAccount/RegisterAccount";
import Page404 from "./Page404/Page404";

function App() {
	return (
		<BrowserRouter>
			<Routes>
				<Route path="/" element={<HomePage />} />
				<Route path="/login" element={<LoginAccount />} />
				<Route path="/register" element={<RegisterAccount />} />
				<Route path="*" element={<Page404 />} />
			</Routes>
		</BrowserRouter>
	);
}

export default App;
